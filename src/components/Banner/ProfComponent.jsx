import React, { useState } from "react";
import styled from "styled-components";
import { AiOutlineInstagram } from "react-icons/ai";
import { FaLinkedinIn,FaFacebook } from "react-icons/fa";
import { Slide } from "react-awesome-reveal";
import { Name,Designation,Expertiese,shortDetails,Details } from "../data";

const ProfComponent = () => {
  const [showFullText,setShowFulltext] = useState(false)
  const linkDesign = {
    textDecoration:"underline",
    cursor:"pointer"
  }
  return (
    <Container id="home">
      <Slide direction="left" style={{width:"50%"}}>
        <Texts>
          <h4>
            Hello <span className="green">I'am</span>
          </h4>
          <h1 className="green">{Name}</h1>
          <h3>{Designation}</h3>
          <p>
              <div className=''>
                  <span>Experience : </span><span>3+ years</span>
              </div>
              <br/>
              <div className=''>
                  <span>Experties in : </span><span>{Expertiese}</span>
              </div>
              <br/>
              <div className=''>
                  <span>Details : </span>{
                  showFullText?
                  <i>{Details} <a style={linkDesign} onClick={()=>setShowFulltext(false)}>Read Less</a></i>:
                  <i>{shortDetails} <a onClick={()=>setShowFulltext(true)} style={linkDesign}>Read More</a></i>}
              </div>
          </p>
          <a href="#footer"><button >Let's talk</button></a> 
          <Social>
            <p>Check out my</p>
            <div className="social-icons">
              <span>
                <a href="https://instagram.com/advocate_shahid_khan?igshid=ZGUzMzM3NWJiOQ==" target="_blank">
                  <AiOutlineInstagram />
                </a>
              </span>
              <span>
                <a href="https://www.facebook.com/Shahidkhansdr?mibextid=ZbWKwL" target="_blank">
                  <FaFacebook />
                </a>
              </span>
              <span>
                <a href="https://www.linkedin.com/in/shahid-khan-85a649253" target="_blank">
                  <FaLinkedinIn />
                </a>
              </span>
            </div>
          </Social>
        </Texts>
      </Slide>
      <Slide direction="right">
        <Profile>
          <img
            src="Protfolio-removebg.png"
            alt="profile"
            width="90px"
          />
        </Profile>
      </Slide>
    </Container>
  );
};

export default ProfComponent;

const Container = styled.div`
  display: flex;
  gap: 2rem;
  padding-top: 3rem;
  width: 80%;
  max-width: 1280px;
  margin: 0 auto;
  z-index: 1;
  @media (max-width: 840px) {
    width: 90%;
  }

  @media (max-width: 640px) {
    flex-direction: column;
  }
`;

const Texts = styled.div`
  
  flex: 1;
  h4 {
    padding: 1rem 0;
    font-weight: 500;
  }
  h1 {
    font-size: 2rem;
    font-family: "Secular One", sans-serif;
    letter-spacing: 2px;
  }
  h3 {
    font-weight: 500;
    font-size: 1.2rem;
    padding-bottom: 1.2rem;
    text-transform: capitalize;
  }
  p {
    font-weight: 300;
  }

  button {
    padding: 0.7rem 2rem;
    margin-top: 3rem;
    cursor: pointer;
    background-color: #01be96;
    border: none;
    color: #fff;
    font-weight: 500;
    filter: drop-shadow(0px 10px 10px #01be9551);
    :hover {
      filter: drop-shadow(0px 10px 10px #01be9570);
    }
  }
`;
const Social = styled.div`
  margin-top: 3rem;
  display: flex;
  align-items: center;
  gap: 1rem;
  p {
    font-size: 0.9rem;
    @media (max-width: 690px) {
      font-size: 0.7rem;
    }
  }

  .social-icons {
    display: flex;
    align-items: center;
    gap: 1rem;
    span {
      width: 2.3rem;
      height: 2rem;
      clip-path: polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%);
      background-color: #01be96;
      position: relative;
      transition: transform 400ms ease-in-out;
      :hover {
        transform: rotate(360deg);
      }
    }

    a {
      color: #fff;
      position: absolute;
      top: 55%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
  }
`;
const Profile = styled.div`
  img {
    width: 25rem;
    filter: drop-shadow(0px 10px 10px #01be9570);
    transition: transform 400ms ease-in-out;
    @media (max-width: 790px) {
      width: 20rem;
    }

    @media (max-width: 660px) {
      width: 18rem;
    }

    @media (max-width: 640px) {
      width: 100%;
    }
  }

  :hover img {
    transform: translateY(-10px);
  }
`;
