export const Name = 'Shahid Khan'
export const Designation = 'Criminal Lawyer'
export const Expertiese = 'Criminal Law, Finance, Civil Laws, Divorce Consulting, Consumer Court, Property Verification'
export const shortDetails = `Results-driven Corporate and Criminal Lawyer with years of experience ensuring the legality of commercial transactions. Adept at drafting and reviewing policies, and client negotiation............`
export const Details = `Results-driven Corporate and Criminal Lawyer with years of experience ensuring the legality of commercial transactions. Adept at drafting and reviewing policies, and client negotiation. Achieved advantageous settlements in 95% of cases.
A highly Capable of analysing complex research, data and documentation to prepare and represent individuals in sensitive cases. Proficient in conducting research and analysing a case to determine a procable outcome and devise an effective strategy to defend the clients in court. Adept in interpreting laws for clients and helping them to understand their legal options and aiming to resolve cases as quickly and favourably as possible. Known for representing clients at arguments, hearings, and court trials.`
export const Email="advocateshahidkhan0070@gmail.com"
export const Phone="+91 8423090070"
export const instaLink="https://instagram.com/advocate_shahid_khan?igshid=ZGUzMzM3NWJiOQ=="
export const fbLink="https://www.facebook.com/Shahidkhansdr?mibextid=ZbWKwL"
export const linkedIn="https://www.linkedin.com/in/shahid-khan-85a649253"
export const Address="Civil Court Siddharth Nagar,272206"
export const ServiceData =[
    {
        title:'MACT : Motor Accident Claim Tribunal',
        icon:'',
        description:'Clause 165 empowers the State Government to constitute Claims Tribunals to adjudicate upon claims for compensation arising out of motor vehicle accidents, resulting in death or bodily injury to persons or damages to any property of third parties'
    },
    {
        title:'Family Law',
        icon:'',
        description:'family law, body of law regulating family relationships, including marriage and divorce, the treatment of children, and related economic matters.'
    },
    {
        title:'Consumer Court',
        icon:'',
        description:'If consumer rights are infringed by a company or business or person, you must take them to task by filing your case in the consumer court.'
    },
]

export const RecentWorkData =[
    {
        title:'MACT : Motor Accident Claim Tribunals',
        icon:'',
        description:'Clause 165 empowers the State Government to constitute Claims Tribunals to adjudicate upon claims for compensation arising out of motor vehicle accidents, resulting in death or bodily injury to persons or damages to any property of third parties'
    },
    {
        title:'Family Law',
        icon:'',
        description:'family law, body of law regulating family relationships, including marriage and divorce, the treatment of children, and related economic matters.'
    },
    {
        title:'Consumer Court',
        icon:'',
        description:'If consumer rights are infringed by a company or business or person, you must take them to task by filing your case in the consumer court.'
    },
]

export const OurTeamData =[
    {
        name:'Sanjay Kumar Pathak',
        img_url : 'lawyer-icon.png',//"https://t4.ftcdn.net/jpg/02/90/27/39/360_F_290273933_ukYZjDv8nqgpOBcBUo5CQyFcxAzYlZRW.jpg",
        stars : 5,
        position:'Senior Counsel',
        disc:'Expertiese in dealing with criminal laws,bails,civil laws,family laws etc.'
    },
    {
        name:'Akhlaq Ahmad',
        img_url :'lawyer-icon.png',
        stars : 3,
        position:'Criminal Law,Finance Law etc',
        disc:'Expertiese in dealing with criminal laws,bails,civil laws,family laws etc.'
    },
    {
        name:'Alok',
        img_url : 'lawyer-icon.png',
        stars : 3,
        position:'Criminal Law,Finance Law etc',
        disc:'Expertiese in dealing with criminal laws,bails,civil laws,family laws etc.'
    },
]
